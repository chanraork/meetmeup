package repositories

import (
	"fmt"
	"github.com/go-pg/pg"
	"gitlab.com/chanraork/meetmeup/models"
)

type MeetupsRepo struct {
	DB *pg.DB
}

func (m *MeetupsRepo) GetMeetups(filter *models.MeetupFilter, limit *int, offset *int) ([]*models.Meetup, error)  {
	var meetups []*models.Meetup
	query := m.DB.Model(&meetups).Order("id")
	if filter != nil {
		if filter.Name != nil && *filter.Name != "" {
			query.Where("name ILIKE ?", fmt.Sprintf("%%%s%%", *filter.Name))
		}
	}
	if limit != nil {
		query.Limit(*limit)
	}
	if offset != nil {
		query.Offset(*offset)
	}
	err := query.Select()
	if err != nil {
		return nil, err
	}
	return meetups, err
}

func (m *MeetupsRepo) CreateMeetup(meetup *models.Meetup) (*models.Meetup, error) {
	_, err := m.DB.Model(meetup).Returning("*").Insert()
	if err != nil {
		return nil, err
	}
	return meetup, nil
}