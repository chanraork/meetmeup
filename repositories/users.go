package repositories

import (
	"errors"
	"fmt"
	"github.com/go-pg/pg"
	"gitlab.com/chanraork/meetmeup/models"
)

type UsersRepo struct {
	DB *pg.DB
}

func (u *UsersRepo) GetUsers() ([]*models.Users, error)  {
	var users []*models.Users
	err := u.DB.Model(&users).Select()
	if err != nil {
		return nil, err
	}
	return users, err
}

func (u *UsersRepo) GetUserById(id string) (*models.Users, error)  {
	return u.GetUserByField("id", id)
}

func (u *UsersRepo) GetUserByField(field, value string) (*models.Users, error) {
	var user models.Users
	err := u.DB.Model(&user).Where(fmt.Sprintf("%v = ?", field), value).First()
	return &user, err
}

func (u *UsersRepo) CreateUser(user *models.Users) (*models.Users, error) {
	_, err := u.DB.Model(user).Returning("*").Insert()
	if err != nil {
		return nil, err
	}
	return user, nil
}

func (u *UsersRepo) UpdateUser(obj *models.Users) (*models.Users, error) {
	fmt.Printf("User : %v\n", obj)
	user, _ := u.GetUserByField("id", obj.ID)
	if user == nil {
		return nil, errors.New("user not exist")
	}
	_, err := u.DB.Model(user).Set("username = ?, email = ?", obj.Username, obj.Email).Where("id = ?", obj.ID).Returning("*").Update()
	if err != nil {
		return nil, err
	}
	return user, nil
}

func (u *UsersRepo) DeleteUser(input models.UserID) (*models.Response, error) {
	user, _ := u.GetUserByField("id", input.ID)
	fmt.Printf("User : %v\n", user)
	if user == nil {
		return &models.Response{
			Code:    404,
			Message: "user not found",
		}, nil
	}
	_, err := u.DB.Model(user).Where("id = ?", input.ID).Delete()
	if err != nil {
		return &models.Response{
			Code:    502,
			Message: "delete user failed",
		}, err
	}
	return &models.Response{
		Code:    200,
		Message: "user was deleted successful",
	}, nil
}

func (u *UsersRepo) GetUserByEmail(email string) (*models.Users, error) {
	return u.GetUserByField("email", email)
}

func (u *UsersRepo) GetUserByUsername(username string) (*models.Users, error) {
	return u.GetUserByField("username", username)
}

func (u *UsersRepo) RegisterUser(tx *pg.Tx, user *models.Users) (*models.Users, error)  {
	_, err := u.DB.Model(user).Returning("*").Insert()
	return user, err
}