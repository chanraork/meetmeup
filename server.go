package main

import (
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-pg/pg"
	"github.com/rs/cors"
	"gitlab.com/chanraork/meetmeup/graph/dataloaders"
	"gitlab.com/chanraork/meetmeup/graph/resolvers"
	"gitlab.com/chanraork/meetmeup/middlewares"
	"gitlab.com/chanraork/meetmeup/postgres"
	"gitlab.com/chanraork/meetmeup/repositories"
	"log"
	"net/http"
	"os"

	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/99designs/gqlgen/graphql/playground"
	"gitlab.com/chanraork/meetmeup/graph/generated"
)

const defaultPort = "8080"

func main() {
	DB := postgres.New(&pg.Options{
		Addr:                  "127.0.0.1:5432",
		User:                  "postgres",
		Password:              "Cr@981993",
		Database:              "meetmeup_db",
	})

	defer DB.Close()

	DB.AddQueryHook(postgres.DBLogger{})

	port := os.Getenv("PORT")
	if port == "" {
		port = defaultPort
	}

	userRepo := repositories.UsersRepo{DB:DB}

	router := chi.NewRouter()

	router.Use(cors.New(cors.Options{
		AllowedOrigins:         []string{"http://localhost:8000"},
		AllowCredentials:       true,
		Debug:                  true,
	}).Handler)
	router.Use(middleware.RequestID)
	router.Use(middleware.Logger)
	router.Use(middlewares.AuthMiddleware(userRepo))

	c := generated.Config{Resolvers: &resolvers.Resolver{
		MeetupsRepo: repositories.MeetupsRepo{DB:DB},
		UsersRepo: userRepo,
	}}

	srv := handler.NewDefaultServer(generated.NewExecutableSchema(c))

	router.Handle("/", playground.Handler("GraphQL playground", "/query"))
	router.Handle("/query", dataloaders.DataloaderMiddleware(DB, srv))

	log.Printf("connect to http://localhost:%s/ for GraphQL playground", port)
	log.Fatal(http.ListenAndServe(":"+port, router))
}
