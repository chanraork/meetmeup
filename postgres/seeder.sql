INSERT INTO users(username, email) VALUES ('Jonh', 'jonh@gmail.com');
INSERT INTO users(username, email) VALUES ('Tom', 'tom@gmail.com');
INSERT INTO users(username, email) VALUES ('Jane', 'jane@gmail.com');

INSERT INTO meetups(name, description, user_id) VALUES ('My first meetup', 'This is a description', 1);
INSERT INTO meetups(name, description, user_id) VALUES ('My second meetup', 'This is a description', 1);
