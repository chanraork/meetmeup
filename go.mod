module gitlab.com/chanraork/meetmeup

go 1.14

require (
	github.com/99designs/gqlgen v0.11.3
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-chi/chi v4.0.4+incompatible // indirect
	github.com/go-pg/pg v8.0.6+incompatible
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/pkg/errors v0.8.1
	github.com/rs/cors v1.7.0 // indirect
	github.com/vektah/dataloaden v0.3.0 // indirect
	github.com/vektah/gqlparser/v2 v2.0.1
	golang.org/x/crypto v0.0.0-20191011191535-87dc89f01550
	mellium.im/sasl v0.2.1 // indirect
)
