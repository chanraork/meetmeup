package dataloaders

import (
	"context"
	"github.com/go-pg/pg"
	"gitlab.com/chanraork/meetmeup/models"
	"net/http"
	"time"
)

const userloaderKey  = "userloader"

func DataloaderMiddleware(db *pg.DB, next http.Handler) http.Handler  {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		userloader := UserLoader{
			maxBatch: 100,
			wait: 1 * time.Millisecond,
			fetch: func(ids []string) ([]*models.Users, []error) {
				var users []*models.Users
				err := db.Model(&users).Where("id in (?)", pg.In(ids)).Select()

				if err != nil {
					return users, []error{err}
				}
				u := make(map[string]*models.Users, len(users))
				for _, user := range users {
					u[user.ID] = user
				}
				result := make([]*models.Users, len(ids))

				for i, id := range ids {
					result[i] = u[id]
				}
				return result, nil
			},
		}
		ctx := context.WithValue(r.Context(), userloaderKey, &userloader)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

func GetUserLoader(ctx context.Context) *UserLoader  {
	return ctx.Value(userloaderKey).(*UserLoader)
}
