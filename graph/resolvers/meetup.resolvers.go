package resolvers

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"

	"gitlab.com/chanraork/meetmeup/graph/dataloaders"
	"gitlab.com/chanraork/meetmeup/graph/generated"
	"gitlab.com/chanraork/meetmeup/models"
)

func (r *meetupResolver) User(ctx context.Context, obj *models.Meetup) (*models.Users, error) {
	return dataloaders.GetUserLoader(ctx).Load(obj.UserId)
}

// Meetup returns generated.MeetupResolver implementation.
func (r *Resolver) Meetup() generated.MeetupResolver { return &meetupResolver{r} }

type meetupResolver struct{ *Resolver }
