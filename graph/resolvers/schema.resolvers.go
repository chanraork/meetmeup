package resolvers

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"errors"
	"fmt"
	"gitlab.com/chanraork/meetmeup/middlewares"
	"log"

	"gitlab.com/chanraork/meetmeup/graph/generated"
	"gitlab.com/chanraork/meetmeup/models"
)

var (
	ErrUnauthenticated = errors.New("unauthenticated")
)

func (r *mutationResolver) CreateMeetup(ctx context.Context, input models.NewMeetup) (*models.Meetup, error) {
	currentUser, err := middlewares.GetCurrentUserFromCTX(ctx)
	if err != nil {
		return nil, ErrUnauthenticated
	}
	if len(input.Name) < 3 {
		return nil, errors.New("name not long enough")
	}
	if len(input.Description) < 3 {
		return nil, errors.New("description not long enough")
	}
	meetup := &models.Meetup{
		Name:        input.Name,
		Description: input.Description,
		UserId: currentUser.ID,
	}

	return r.MeetupsRepo.CreateMeetup(meetup)
}

func (r *mutationResolver) Register(ctx context.Context, input models.RegisterInput) (*models.AuthResponse, error) {
	_, err := r.UsersRepo.GetUserByEmail(input.Email)
	if err == nil {
		return nil, errors.New("email already in used")
	}

	_, err = r.UsersRepo.GetUserByUsername(input.Username)
	if err == nil {
		return nil, errors.New("username already in used")
	}

	user := &models.Users{
		Username:  input.Username,
		Email:     input.Email,
		FirstName: input.FirstName,
		LastName:  input.LastName,
	}

	err = user.HashPassword(input.Password)
	if err != nil {
		log.Print("error while hashing password : %v", err)
		return nil, errors.New("something went wrong")
	}

	// TODO: create verification code
	tx, err := r.UsersRepo.DB.Begin()
	if err != nil {
		log.Printf("error creating a transaction : %v", err)
		return nil, errors.New("something went wrong")
	}
	defer tx.Rollback()

	if _, err := r.UsersRepo.RegisterUser(tx, user); err != nil {
		log.Printf("error creating a user : %v", err)
		return nil, err
	}
	if err := tx.Commit(); err != nil {
		log.Printf("error while commiting : %v", err)
		return nil, err
	}

	token, err := user.GenerateToken()
	if err != nil {
		log.Printf("error while generating a token : %v", err)
		return nil, errors.New("something went wrong")
	}

	return &models.AuthResponse{
		AuthToken: token,
		User:      user,
	}, nil
}

func (r *mutationResolver) Login(ctx context.Context, input models.LoginInput) (*models.AuthResponse, error) {
	user, err := r.UsersRepo.GetUserByEmail(input.Email)
	if err != nil {
		return nil, ErrBadCredential
	}

	err = user.ComparePassword(input.Password)
	if err != nil {
		return nil, ErrBadCredential
	}

	token, err := user.GenerateToken()
	if err != nil {
		return nil, errors.New("something went wrong")
	}

	return &models.AuthResponse{
		AuthToken: token,
		User:      user,
	}, nil
}

func (r *mutationResolver) CreateUser(ctx context.Context, input models.NewUser) (*models.Users, error) {
	user := &models.Users{
		Username: input.Username,
		Email:    input.Email,
	}
	return r.UsersRepo.CreateUser(user)
}

func (r *mutationResolver) UpdateUser(ctx context.Context, input models.UserID, obj models.NewUser) (*models.Users, error) {
	user := &models.Users{
		ID:       input.ID,
		Username: obj.Username,
		Email:    obj.Email,
	}
	return r.UsersRepo.UpdateUser(user)
}

func (r *mutationResolver) DeleteUser(ctx context.Context, input models.UserID) (*models.Response, error) {
	return r.UsersRepo.DeleteUser(input)
}

func (r *queryResolver) Meetups(ctx context.Context, filter *models.MeetupFilter, limit *int, offset *int) ([]*models.Meetup, error) {
	return r.MeetupsRepo.GetMeetups(filter, limit, offset)
}

func (r *queryResolver) Users(ctx context.Context) ([]*models.Users, error) {
	return r.UsersRepo.GetUsers()
}

func (r *queryResolver) GetUserByID(ctx context.Context, id string) (*models.Users, error) {
	panic(fmt.Errorf("not implemented"))
}

// Mutation returns generated.MutationResolver implementation.
func (r *Resolver) Mutation() generated.MutationResolver { return &mutationResolver{r} }

// Query returns generated.QueryResolver implementation.
func (r *Resolver) Query() generated.QueryResolver { return &queryResolver{r} }

type mutationResolver struct{ *Resolver }
type queryResolver struct{ *Resolver }

// !!! WARNING !!!
// The code below was going to be deleted when updating resolvers. It has been copied here so you have
// one last chance to move it out of harms way if you want. There are two reasons this happens:
//  - When renaming or deleting a resolver the old code will be put in here. You can safely delete
//    it when you're done.
//  - You have helper methods in this file. Move them out to keep these resolver files clean.
var (
	ErrBadCredential = errors.New("email/password combination don't work")
)

func (r *mutationResolver) GetUserByID(ctx context.Context, input models.UserID) (*models.Users, error) {
	return r.UsersRepo.GetUserById(input.ID)
}
