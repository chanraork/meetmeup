//go:generate go run github.com/99designs/gqlgen
package resolvers

import "gitlab.com/chanraork/meetmeup/repositories"

// This file will not be regenerated automatically.
//
// It serves as dependency injection for your app, add any dependencies you require here.

type Resolver struct{
	MeetupsRepo repositories.MeetupsRepo
	UsersRepo repositories.UsersRepo
}
