package resolvers

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"fmt"

	"gitlab.com/chanraork/meetmeup/graph/generated"
	"gitlab.com/chanraork/meetmeup/models"
)

func (r *usersResolver) Meetups(ctx context.Context, obj *models.Users) ([]*models.Meetup, error) {
	return nil, nil
}

// Users returns generated.UsersResolver implementation.
func (r *Resolver) Users() generated.UsersResolver { return &usersResolver{r} }

type usersResolver struct{ *Resolver }

// !!! WARNING !!!
// The code below was going to be deleted when updating resolvers. It has been copied here so you have
// one last chance to move it out of harms way if you want. There are two reasons this happens:
//  - When renaming or deleting a resolver the old code will be put in here. You can safely delete
//    it when you're done.
//  - You have helper methods in this file. Move them out to keep these resolver files clean.
func (r *usersResolver) Status(ctx context.Context, obj *models.Users) (bool, error) {
	panic(fmt.Errorf("not implemented"))
}
