package middlewares

import (
	"context"
	"github.com/dgrijalva/jwt-go"
	"github.com/dgrijalva/jwt-go/request"
	"github.com/pkg/errors"
	"gitlab.com/chanraork/meetmeup/models"
	"gitlab.com/chanraork/meetmeup/repositories"
	"net/http"
	"strings"
)

const CurrentUserKey = "currentUser"

func AuthMiddleware(repo repositories.UsersRepo) func(handler http.Handler) http.Handler  {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			token, err := parseToken(r)
			if err != nil {
				next.ServeHTTP(w, r)
				return
			}
			claims, ok := token.Claims.(jwt.MapClaims)
			if !ok || !token.Valid {
				next.ServeHTTP(w, r)
				return
			}

			user, err := repo.GetUserById(claims["jti"].(string))
			if err != nil {
				next.ServeHTTP(w, r)
				return
			}

			ctx := context.WithValue(r.Context(), CurrentUserKey , user)

			next.ServeHTTP(w, r.WithContext(ctx))
		})
	}
}

var authHeaderExtractor = &request.PostExtractionFilter{
	Extractor: &request.HeaderExtractor{"Authorization"},
	Filter:    stripBearerPrefixToken,
}

func stripBearerPrefixToken(token string) (string, error) {
	bearer := "BEARER"

	if len(token) > len(bearer) && strings.ToUpper(token[0:len(bearer)]) == bearer {
		return token[len(bearer) + 1:], nil
	}

	return token, nil
}
var authExtractor = &request.MultiExtractor{
	authHeaderExtractor,
	&request.ArgumentExtractor{"access_token"},
}

func parseToken(r *http.Request) (*jwt.Token, error) {
	jwtToken, err := request.ParseFromRequest(r, authExtractor, func(token *jwt.Token) (interface{}, error) {
		var jwtSecret = "jwtsecret"

		t := []byte(jwtSecret)
		return t, nil
	})
	return jwtToken, errors.Wrap(err, "parseToken error:")
}

func GetCurrentUserFromCTX(ctx context.Context) (*models.Users, error) {
	if ctx.Value(CurrentUserKey) == nil {
		return nil, errors.New("no user in context")
	}

	user, ok := ctx.Value(CurrentUserKey).(*models.Users)
	if !ok || user.ID == "" {
		return nil, errors.New("no user in context")
	}

	return user, nil
}