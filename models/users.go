package models

import (
	"github.com/dgrijalva/jwt-go"
	"golang.org/x/crypto/bcrypt"
	"time"
)

type Users struct {
    ID string `json:"id"`
    Username string `json:"username"`
    Email string `json:"email"`
    Password string `json:"password"`
    FirstName string `json:"firstName"`
    LastName string `json:"lastName"`
    CreatedAt time.Time `json:"createdAt"`
    UpdatedAt time.Time `json:"updatedAt"`
    DeletedAt *time.Time `json:"-" pg:",soft_delete"`
}

func (u *Users) HashPassword(password string) error {
	bytePassword := []byte(password)
	passwordHash, err := bcrypt.GenerateFromPassword(bytePassword, bcrypt.DefaultCost)

	if err != nil {
	    return err
    }

    u.Password = string(passwordHash)

    return nil
}

func (u *Users) GenerateToken() (*AuthToken, error) {
	expiredAt := time.Now().Add(time.Hour * 24 * 7) // a week
	var jwtSecret = "jwtsecret"

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.StandardClaims{
		ExpiresAt: expiredAt.Unix(),
		Id:        u.ID,
		IssuedAt:  time.Now().Unix(),
		Issuer:    "meetmeup",
	})

	accessToken, err := token.SignedString([]byte(jwtSecret))
	if err != nil {
		return nil, err
	}

	return &AuthToken{
		AccessToken: accessToken,
		ExpiredAt:   expiredAt,
	}, nil
}

func (u *Users) ComparePassword(password string) error {
	bytePassword := []byte(password)
	byteHashPassword := []byte(u.Password)
	return bcrypt.CompareHashAndPassword(byteHashPassword, bytePassword)
}